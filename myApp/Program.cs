﻿using System;

namespace myApp
{
    public class Program
    {
        static void Main()
        {
            Program helloApp = new Program();
            helloApp.say_hello();
            helloApp.say_bye();            
        }

        public void say_hello()
        {
            try{
                Console.WriteLine(Figgle.FiggleFonts.Standard.Render("Hello, World!"));
                //Console.WriteLine("The current time is " + DateTime.Now);
                //Console.WriteLine(result);
            }catch (Exception ex){
                Console.Error.WriteLine(ex.Message);
            }
        }

        public void say_bye()
        {
            try{
                Console.WriteLine(Figgle.FiggleFonts.Standard.Render("Bye, World!"));
                //Console.WriteLine("The current time is " + DateTime.Now);
                //Console.WriteLine(result);
            }catch (Exception ex){
                Console.Error.WriteLine(ex.Message);
            }
        }

    }
}
