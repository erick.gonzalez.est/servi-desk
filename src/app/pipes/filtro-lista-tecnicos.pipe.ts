import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroListaTecnicos',
})
export class FiltroListaTecnicosPipe implements PipeTransform {
  transform(arreglo: any[], texto: string): any[] {
    console.log(texto);
    if (texto === '') {
      return arreglo;
    }

    console.log(arreglo)
    return arreglo.filter((item) => {
      if (item != null) {
        return item.nombres
          .toLowerCase()
          .trim()
          .includes(texto);
      } else {
        return null;
      }
    });
  }
}
