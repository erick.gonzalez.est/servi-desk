import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(arreglo: any[], texto: string, data:string): any[] {
    if(texto===''){
      return arreglo;
    }
    console.log(data);
    
    texto=texto.toString().toLowerCase().trim();
    if(data=="usuario"){
      return arreglo.filter(item=>{
        return item.usuario.persona.nombres.toLowerCase().trim().includes(texto) ;
     })
    }
    if(data=="tecnico"){
      return arreglo.filter(item=>{
        if(item.tecnico!=null){
          return item.tecnico.persona.nombres.toLowerCase().trim().includes(texto);
        }else{
          return null;
        }
        
     })
    } 
    if(data=="TipoServicio"){
      return arreglo.filter(item=>{
        return item.servicio.tipo.nombre.toLowerCase().trim().includes(texto)  ;
     })
    }
    if(data=="criticidad"){
      return arreglo.filter(item=>{
        return item.servicio.criticidad.nivelCriticidad.toLowerCase().trim().includes(texto) ;
     })
    }
    if(data=="Servicio"){
      return arreglo.filter(item=>{
        return item.servicio.descripcion.toLowerCase().trim().includes(texto);
     })
    }
    if(data=="severidad"){
      return arreglo.filter(item=>{
        return item.severidad.nivelSeveridad.toLowerCase().trim().includes(texto) ;
     })
    }
    if(data=="prioridad"){
      return arreglo.filter(item=>{
        console.log("prioridadddd")
        return item.servicio.prioridad.nivelPrioridad.toLowerCase().trim().includes(texto) ;
     })
    }
  }

}
