import { FiltroPipe } from './filtro.pipe';
import { NgModule } from '@angular/core';
import { FiltroListaTecnicosPipe } from './filtro-lista-tecnicos.pipe';
import { FiltroTPipe } from './filtro-t.pipe';
import { FiltroListaEstadoTicketsPipe } from './filtro-lista-estado-tickets.pipe';


@NgModule({
  declarations: [FiltroPipe, FiltroListaTecnicosPipe, FiltroListaEstadoTicketsPipe,FiltroTPipe],
  exports:[FiltroPipe,FiltroListaTecnicosPipe,FiltroListaEstadoTicketsPipe,FiltroTPipe]

})
export class PipesModule { }
