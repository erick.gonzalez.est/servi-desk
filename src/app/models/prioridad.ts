export interface prioridad{
    codPrioridad : number;
    nivelPrioridad : string;
    tiempoRespuesta : number;
}