export interface Persona {
    cedula:string;
    nombres:string;
    telefono:string;
    direccion:string;
}
