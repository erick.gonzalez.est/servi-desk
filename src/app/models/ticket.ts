export interface Ticket {
    codTicket: any;
    codCatalogo:any;
    codServicio:any;
    descripcionTicket: any;
    codSeveridad: any;
    codViaComunicacion: any;
    codUsuario: any;
    sla: any;
    url: any;
    fechaCreacion: any;
    estado: any;
}
