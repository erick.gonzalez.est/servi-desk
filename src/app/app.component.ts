import { Component } from '@angular/core';
import { AlertController, Platform } from '@ionic/angular';
// import { SplashScreen } from '@ionic-native/splash-screen/ngx';
// import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UtilsService } from './utils/utils.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public usuarioPages = [
    { title: 'Editar perfil', url: '/registro', icon: 'archive' },
  ];

  public tecnicoPages = [
    
  ];

  public coordinadorPages = [
    { title: 'Incidentes', url: '/lista-ticket-coordinador/Incidente', icon: 'build' },
    { title: 'Requerimientos', url: '/lista-ticket-coordinador/Requerimiento', icon: 'document-text' },
    { title: 'Eliminados', url: '/tickets-eliminados-coordinador', icon: 'trash' },
    { title: 'Rechazados', url: '/tickets-rechazados-coordinador', icon: 'close-circle' },
  ];

  tipo: any;
  constructor(
    private utils: UtilsService,
    private platform: Platform,
    // private splashScreen: SplashScreen,
    // private statusBar: StatusBar,
    private alertCrtl: AlertController,
    private router: Router
  ) {
    localStorage.setItem('userEditable', '1');
    this.tipo = localStorage.getItem('TipoUsuario');
    console.log('Tipo: ' + this.tipo);
  }

  ngOnInit() {
    localStorage.setItem('userEditable', '1');
    this.tipo = localStorage.getItem('TipoUsuario');
    console.log('Tipo: ' + this.tipo);
  }

  cambio(){
    this.ngOnInit();
  }

  updateTicket() {
    localStorage.setItem('userEditable', '1');
    this.tipo = localStorage.getItem('TipoUsuario');
    console.log('Tipo: ' + this.tipo);
  }

  async cerrarSesion() {
    //  this.router.navigateByUrl('/home');
    const toast = await this.alertCrtl.create({
      header: 'Cerrar Sesion',
      message: '¿Esta seguro de que desea cerrar sesion?',
      //position: 'middle',
      buttons: [
        {
          text: 'Cancelar',
          role: 'no',
          cssClass: 'light',
          handler: () => {
            console.log('cancel clicked');
          },
        },
        {
          text: 'Salir',
          handler: () => {
            localStorage.clear();
            this.router.navigateByUrl('/login');
            console.log('leave clicked');
          },
        },
      ],
    });
    toast.present();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // this.statusBar.styleDefault();
      // this.splashScreen.hide();
    });
  }
  persist() {
    this.platform.backButton.subscribe(() => {
      console.log('Test button');
      // this.salirApp();
    });
  }

  async salirApp() {
    const alert = await this.alertCrtl.create({
      header: 'Confirmar!',
      message: '¿Deseas Cerrar la Aplicacion?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'no',
          cssClass: 'light',
          handler: () => {
            console.log('Confirmar no');
          },
        },
        {
          text: 'Salir',
          handler: () => {
            navigator['app'].exitApp();
            console.log('Cancel clicked');
          },
        },
      ],
    });
    alert.present();
    // if (localStorage.getItem('cedulaTemporal') != null) {
    //   localStorage.removeItem('cedulaTemporal');
    // }
  }
}
