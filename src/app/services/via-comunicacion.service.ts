import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilsService } from '../utils/utils.service';

@Injectable({
  providedIn: 'root'
})
export class ViaComunicacionService {

  // url = 'http://192.168.1.50:9898/viaComunicacion/';
  url = "https://servi-desk.herokuapp.com/viaComunicacion/"


  constructor(private http: HttpClient, private toast:UtilsService) { }

  getViaComunicacion() {
    return new Promise(resolve => {
      this.http.get(this.url+'listar').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
}