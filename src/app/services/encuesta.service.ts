import { Encuesta } from 'src/app/models/encuesta';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EncuestaService {
  url = "https://servi-desk.herokuapp.com/detalleEncuesta/"
  constructor(private http:HttpClient) { }

  dataEncuesta(cod_encuesta:any):Observable<any>{
    return this.http.get(`${this.url}preguntas/`+cod_encuesta);
  }
}
