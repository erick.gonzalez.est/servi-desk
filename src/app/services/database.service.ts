import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLiteObject,SQLite } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
import { BehaviorSubject, Observable } from 'rxjs';
import { Ticket } from '../models/ticket';
import { usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  private database: SQLiteObject;
  private dbReady = new BehaviorSubject<boolean>(false);

  usuarios = new BehaviorSubject([]);
  tickets = new BehaviorSubject([]);

  constructor(private plt: Platform, private sqlitePorter: SQLitePorter, private sqlite: SQLite, private http: HttpClient) { 
    this.plt.ready().then(() => {
      this.sqlite.create({
        name: 'servidesk.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
          this.database = db;
          this.seedDatabase();
      });
    });
    
  }

  seedDatabase() {
    this.http.get('assets/seed.sql', { responseType: 'text'})
    .subscribe(sql => {
      this.sqlitePorter.importSqlToDb(this.database, sql)
        .then(_ => {
          this.loadTickets();
          this.loadUsuarios();
          this.dbReady.next(true);
        })
        .catch(e => console.error(e));
    });
  }

  getDatabaseState() {
    return this.dbReady.asObservable();
  }
 
  getUsuarios(): Observable<any[]> {
    return this.usuarios.asObservable();
  }
 
  getTickets(): Observable<any[]> {
    return this.tickets.asObservable();
  }



  async loadUsuarios() {
  let query = 'SELECT * from usuario';
  const data = await this.database.executeSql(query, []);
  let usuarios = [];
  if (data.rows.length > 0) {
    for (var i = 0; i < data.rows.length; i++) {
      usuarios.push({
        codUsuario: data.rows.item(i).codUsuario,
        email: data.rows.item(i).email,
        contrasena: data.rows.item(i).contrasena,
        cedula: data.rows.item(i).cedula,
      });
    }
    console.log(usuarios);
  }
  this.usuarios.next(usuarios);
}

  async loadTickets() {
  let query = 'SELECT * from ticket';
  const data = await this.database.executeSql(query, []);
  let tickets = [];
  if (data.rows.length > 0) {
    for (var i = 0; i < data.rows.length; i++) {
      tickets.push({
        codTicket: data.rows.item(i).codTicket,
        codCatalogo: data.rows.item(i).codCatalogo,
        codServicio: data.rows.item(i).codServicio,
        codSeveridad: data.rows.item(i).codSeveridad,
        descripcionTicket: data.rows.item(i).descripcionTicket,
        codViaComunicacion: data.rows.item(i).codViaComunicacion,
        codUsuario: data.rows.item(i).codUsuario,
        sla: data.rows.item(i).sla,
        url: data.rows.item(i).url,
        fechaCreacion: data.rows.item(i).fechaCreacion,
        estado: data.rows.item(i).estado,
      });
    }
    console.log(tickets);
  }
  this.tickets.next(tickets);
}

  async addTickets(tickets: Ticket) {
  const data = this.database.executeSql("INSERT INTO ticket (codCatalogo, codServicio, codSeveridad, descripcionTicket, codViaComunicacion, codUsuario, sla, url, fechaCreacion, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
    [tickets.codCatalogo, tickets.codServicio, tickets.codSeveridad, tickets.descripcionTicket, tickets.codViaComunicacion, tickets.codUsuario, tickets.sla, tickets.url, tickets.fechaCreacion, tickets.estado]);
  this.loadTickets();
}

  async addUsuarios(usuarios: usuario) {
  console.log("datos recividos en database service" + JSON.stringify(usuarios));
  const data = this.database.executeSql("INSERT INTO usuario (email, cedula, contrasena) VALUES (?, ?, ?)",
    [usuarios.email, usuarios.persona.cedula, usuarios.contrasena]);
  this.loadUsuarios();
}

  async updateUsuarios(usuarios: usuario) {
  let data = [usuarios.email, usuarios.contrasena, usuarios.persona.cedula];
  "UPDATE usuario SET email=?, contrasena=?, WHERE cedula=?"
  const data_1 = await this.database.executeSql(`UPDATE usuario SET email=?, contrasena=? WHERE cedula =?`, data);
  this.loadUsuarios();
}

// async updateTickets(usuarios: usuario) {
//   let data = [usuarios.email, usuarios.contrasena, usuarios.persona.cedula];
//   "UPDATE usuario SET email=?, contrasena=?, WHERE cedula=?"
//   const data_1 = await this.database.executeSql(`UPDATE usuario SET email=?, contrasena=?, WHERE cedula = ${usuarios.persona.cedula}`, data);
//   this.loadUsuarios();
// }
}
