import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TecnicoService {
  url = 'https://servi-desk.herokuapp.com';
  //url = 'http://localhost:9898/';
  results: Observable<any>;
  constructor(private http: HttpClient) { }
  GetTecnicos(nivel:number){
    return new Promise(resolve => {
      this.http.get(this.url+'/persona/obtener_tecnico_nivel/'+nivel).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  GetTecnicoIdForCedula(cedula:String){
    return new Promise(resolve => {
      this.http.get(this.url+'/tecnico/buscar_por_cedula/'+cedula).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
    //http://localhost:9898/persona/obtener_tecnico_nivel/1
    ///tecnico/1
  }
  getTecnicoForId(id:number){
    return new Promise(resolve => {
      this.http.get(this.url+'/tecnico/tecnico/'+id).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
}
