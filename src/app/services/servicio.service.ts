import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServicioService {

  url = "https://servi-desk.herokuapp.com/servicio/"
  // url = 'http://192.168.1.50:9898/servicio/';

  constructor(private http: HttpClient) { }


  getServiciosByTipo(codTipo:number, codCatalogo:number  ){
    return new Promise(resolve => {
      this.http.get(this.url+'filtrar-servicio/'+ codTipo +'/'+ codCatalogo).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  async getServicioByCodigo(codServicio:number){
    return new Promise(resolve => {
      this.http.get(this.url+'buscar-servicio/'+ codServicio).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

}