import { UtilsService } from 'src/app/utils/utils.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SeveridadService {

  url = 'https://servi-desk.herokuapp.com';
 // url = 'http://localhost:9898';

  results: Observable<any>;
  constructor(private http: HttpClient, private toast:UtilsService) { }

  getListSeveridad() {
    return new Promise(resolve => {
      this.http.get(this.url+'/severidad/listar-severidades').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  
  getSeveridadPorNivelSeveridad(severidadNivel:string){
    return new Promise(resolve => {
      this.http.get(this.url+'/severidad/recuperar-severidad-nivel/'+severidadNivel).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  async getSeveridadPorCodSeveridad(codSeveridad:string){
    return new Promise(resolve => {
      this.http.get(this.url+'/severidad/recuperar-severidad/'+codSeveridad).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
//"http://localhost:9898/severidad/recuperar-severidad-nivel/
  putSeveridadTicket(cod_severidad:number,codticket:number){
    //http://localhost:9898/ticket/cambiar_severidad/1/1
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      let options = { headers: headers};
      this.http.put(this.url+'/ticket/cambiar_severidad/'+cod_severidad+'/'+codticket,null,options).subscribe(data => {
        this.toast.presentToastLenin("Nivel de severidad guardada con exito")
        resolve(data);
      }, err => {
        this.toast.presentToastLenin("No se pudo cambiar el nivel de severidad")
        console.log(err);
        
      });
    });
  }
}
