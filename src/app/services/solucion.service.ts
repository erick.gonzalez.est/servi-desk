import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Solucion } from 'src/app/models/solucion';

@Injectable({
  providedIn: 'root'
})
export class SolucionService {
  // url = 'http://192.168.1.50:9898/solucion/'
  url = 'https://servi-desk.herokuapp.com/solucion/'
  constructor(private http:HttpClient) { }

  setSolucion(solucion:any){
    return new Promise(resolve =>{
      let headers = new HttpHeaders({'Content-Type':'application/json'});
      let options = { headers:headers};
      this.http.post(this.url+'guardar',solucion, options).subscribe(data =>{
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getSolucion(codSolucion:any): Observable<any>{
    return this.http.get(`${this.url}buscar/`+codSolucion);
  }
  getSolucionById(id:any) {
    return new Promise(resolve => {
      this.http.get(this.url+'buscar_codticket/'+id).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  
  getSolucionByTicketId(codTicket:number) {
    return new Promise(resolve => {
      this.http.get(this.url+'buscar_codticket/'+codTicket).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

}
