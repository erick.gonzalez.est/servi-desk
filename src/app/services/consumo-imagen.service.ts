import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import * as AWS from 'aws-sdk';
@Injectable({
  providedIn: 'root',
})
export class ConsumoImagenService {
  showImagen = false;
  error = false;
  subiendo = false;
  urlImagen: string;
  submitted = false;
  url: string;
  archivo:File;
  albumBucketName = 'imagenperfil';

  photoSelected: string | ArrayBuffer | null;
  uploadPercent: Observable<number>;
  constructor() {
    AWS.config.region = 'us-east-1'; // Región
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: 'us-east-1:8b65d606-ef9d-484e-820e-e8d5503b8825',
    });
  }
  async onClickSubir () {
    if (this.archivo) {
      try {
        this.verURL('data.Location');
        this.subiendo = true;
        const data = await new AWS.S3.ManagedUpload({
          params: {
            Bucket: this.albumBucketName,
            Key: this.archivo.name,
            Body: this.archivo,
            ACL: 'public-read',
          },
        }).promise();
        this.urlImagen = data.Location;
        this.subiendo = false;
        this.showImagen = true;
        this.verURL(data.Location);
      } catch (error) {
        this.error = true;
        const bucle = setInterval(() => {
          this.error = false;
          clearInterval(bucle);
        }, 2000);
      }
    }
  };
  onChange(file:File){
      console.log(file);
      this.archivo=file;
  }
  verURL(url){
    var params = {
      Bucket: this.albumBucketName,
      Key: this.archivo.name,
      Body: this.archivo,
      ACL: 'public-read',
    }
    localStorage.setItem("url", url);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    console.log(params);
    console.log(url);
  }
}
