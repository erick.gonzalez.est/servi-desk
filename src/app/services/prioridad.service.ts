import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PrioridadService {
  //url = 'http://localhost:9898/';
  url = 'https://servi-desk.herokuapp.com';
  results: Observable<any>;
  constructor(private http: HttpClient) { }

  getIdOfPrioridad(nivelPrioridad:string){
    return new Promise(resolve => {
      this.http.get(this.url+'/prioridad/buscar_id/'+nivelPrioridad).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  putPrioridadTicket(cod_prioridad:number,cod_servicio:number) {
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      let options = { headers: headers};
      this.http.put(this.url+'/servicio/actualizar-servicio/prioridad/'+cod_prioridad+'/'+cod_servicio,null,options).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getListPrioridad() {
    return new Promise(resolve => {
      this.http.get(this.url+'/prioridad/listar').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  
}
