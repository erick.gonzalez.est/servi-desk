import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TicketsEliminadosCoordinadorPage } from './tickets-eliminados-coordinador.page';

const routes: Routes = [
  {
    path: '',
    component: TicketsEliminadosCoordinadorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TicketsEliminadosCoordinadorPageRoutingModule {}
