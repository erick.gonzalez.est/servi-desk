import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TicketsEliminadosCoordinadorPageRoutingModule } from './tickets-eliminados-coordinador-routing.module';

import { TicketsEliminadosCoordinadorPage } from './tickets-eliminados-coordinador.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule, 
    TicketsEliminadosCoordinadorPageRoutingModule
  ],
  declarations: [TicketsEliminadosCoordinadorPage]
})
export class TicketsEliminadosCoordinadorPageModule {}
