import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TicketsEliminadosCoordinadorPage } from './tickets-eliminados-coordinador.page';

describe('TicketsEliminadosCoordinadorPage', () => {
  let component: TicketsEliminadosCoordinadorPage;
  let fixture: ComponentFixture<TicketsEliminadosCoordinadorPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketsEliminadosCoordinadorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TicketsEliminadosCoordinadorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
