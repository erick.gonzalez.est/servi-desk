import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalleTicketUsuarioPage } from './detalle-ticket-usuario.page';

const routes: Routes = [
  {
    path: '',
    component: DetalleTicketUsuarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleTicketUsuarioPageRoutingModule {}
