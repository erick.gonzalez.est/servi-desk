import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  AlertController,
  ModalController,
  NavController,
} from '@ionic/angular';
import { SolucionService } from 'src/app/services/solucion.service';
import { TicketService } from 'src/app/services/ticket.service';
import { UtilsService } from 'src/app/utils/utils.service';

@Component({
  selector: 'app-detalle-ticket-usuario',
  templateUrl: './detalle-ticket-usuario.page.html',
  styleUrls: ['./detalle-ticket-usuario.page.scss'],
})
export class DetalleTicketUsuarioPage implements OnInit {
  codTicket: any;
  ticket: any = [];
  servicios: any = [];
  catalogos: any = [];
  personas: any = [];
  usuarios: any = [];
  viaComunicacion: any = [];
  estado: any = [];
  solucionData: any = [];
  solucionDescripcion: any;
  confirmacion: any;
  constructor(
    private servicioTicket: TicketService,
    private toastService: UtilsService,
    public navCtrl: NavController,
    public alertController: AlertController,
    private modalCtrl: ModalController,
    private solucionService: SolucionService,
    private router: Router
    ) { 
    this.getSolucion(this.codTicket);
    }

  ngOnInit() {
    this.codTicket = localStorage.getItem('codTicket');
    this.getTicketById(this.codTicket);
    this.getSolucion(this.codTicket);
  }

  ionViewWillEnter() {
    this.getTicketById(this.codTicket);
    if (this.confirmacion == true) {
      this.getSolucion(this.codTicket);
    }
  }
  ionViewWillUnload() {
    console.log('se estan borrando los datos');
    this.cerrar();
  }
  popView() {
    this.router.navigateByUrl('/lista-ticket-usuario');
    // this.navCtrl.navigateBack("/lista-ticket-usuario");
  }

  getTicketById(codTicket: number) {
    this.servicioTicket.getTicketById(codTicket).then((data) => {
      this.ticket = data[0];
      localStorage.setItem('ticket', JSON.stringify(this.ticket));
      this.servicios = data[0].servicio;
      this.catalogos = data[0].servicio.catalogo;
      this.personas = data[0].usuario.persona;
      this.usuarios = data[0].usuario;
      this.viaComunicacion = data[0].viaComunicacion;
      this.estado = data[0].estado;
      this.confirmacion = data[0].confirmacion;
    });
  }

  getSolucion(codTicket: number) {
    this.solucionService.getSolucionByTicketId(codTicket).then((data) => {
      this.solucionData = data;
      this.solucionDescripcion = data[0].descripcion;
      console.log('esta es la solucion: ' + this.solucionDescripcion);
    });
  }

  cerrar() {
    localStorage.removeItem('codTicket');
    localStorage.removeItem('ticket');
    localStorage.removeItem('editable');
  }

  async presentAlertaVerDescripcionServicio() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Detalle del Servicio',
      subHeader: 'Descripcion:',
      message: this.servicios.descripcion,
      buttons: ['OK'],
    });

    await alert.present();
  }
  async presentAlertaVerDescripcionTicket() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Detalle del Ticket',
      subHeader: 'Descripcion:',
      message: this.ticket.descripcionTicket,
      buttons: ['OK'],
    });

    await alert.present();
  }

  async presentAlertaRechazar() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Rechazo de Ticket',
      message: '¿Seguro que desea rechazar la atencion de este ticket?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          },
        },
        {
          text: 'OK',
          handler: () => {
            this.cambiarEstado(
              3,
              this.codTicket,
              'Ticket Rechazado',
              'Error al rechazar el ticket'
            );
            this.popView();
          },
        },
      ],
    });
    await alert.present();
  }

  cambiarEstado(cod_estado: number, codTicket: any, txt1: any, txt2: any) {
    this.servicioTicket.putCambiarEstado(cod_estado, codTicket).then(
      (data) => {
        this.toastService.presentToastLenin(txt1);
      },
      (err) => {
        this.toastService.presentToastLenin(txt2);
      }
    );
  }

  async presentAlertaConfirmar() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmar solución',
      message: '¿Seguro que desea confirmar la solucion brindada?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          },
        },
        {
          text: 'OK',
          handler: () => {
            this.cambiarEstado(
              5,
              this.codTicket,
              'Solución Confirmada',
              'Error al confirmar la solucion'
            );
            this.router.navigateByUrl('/lista-ticket-usuario'); // encuesta
          },
        },
      ],
    });
    await alert.present();
  }

  updateTicket() {
    localStorage.setItem('editable', '1');
    // this.navCtrl.navigateForward('registro-ticket');
    this.router.navigateByUrl('/registro-ticket');
  }
}
