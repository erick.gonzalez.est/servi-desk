import { Component, OnInit } from '@angular/core';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { AlertController, ModalController } from '@ionic/angular';
import { Encuesta } from './../../models/encuesta';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.page.html',
  styleUrls: ['./encuesta.page.scss'],
})
export class EncuestaPage implements OnInit {

  encuesta=[];
  constructor(private modalController: ModalController,
    private service: EncuestaService,
    ) { }

  ngOnInit() {
    this.service.dataEncuesta(1).subscribe(
      data =>{
        this.encuesta = data;
        console.log(this.encuesta);

      },err=>{
        console.log(err);

      }
    );
  }

}
