import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PipesModule } from 'src/app/pipes/pipes.module';

import { IonicModule } from '@ionic/angular';

import { ListaTecnicoPageRoutingModule } from './lista-tecnico-routing.module';

import { ListaTecnicoPage } from './lista-tecnico.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    ListaTecnicoPageRoutingModule
  ],
  declarations: [ListaTecnicoPage]
})
export class ListaTecnicoPageModule {}
