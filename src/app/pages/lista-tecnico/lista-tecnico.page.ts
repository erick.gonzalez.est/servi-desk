import { Component, OnInit } from '@angular/core';
import { TicketService } from 'src/app/services/ticket.service';
import { AlertController } from '@ionic/angular';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-lista-tecnico',
  templateUrl: './lista-tecnico.page.html',
  styleUrls: ['./lista-tecnico.page.scss'],
})
export class ListaTecnicoPage implements OnInit {

  ticket = [];
  ticket2:any=[]
  tecnico = Number(window.localStorage["idUsuario"]);
  validate:boolean = false;
  textoBusqueda:string='';
  tipoServicio:string='prioridad';
  defectSelect:string="Buscar por: Servicio";

  constructor(private service: TicketService, public alertController: AlertController, private navCtrl: NavController) { }

  ngOnInit() {
    this.listarTickets();
  }

  ionViewWillEnter(){
    this.listarTickets();
  }

  doRefresh(event){
    this.ngOnInit();
    setTimeout(() => {
      event.target.complete();
    }, 500);
  }
  listarTickets(){
    this.service.dataTicket(this.tecnico).subscribe(data =>{
      this.ticket2 = data;
      this.ticket=this.ticket2.sort(function(a, b) { a = new Date(a.fechaCreacion); b = new Date(b.fechaCreacion); return a>b ? -1 : a<b ? 1 : 0; });
      console.log(this.ticket);
      if (this.ticket.length == 0) {
        this.presentAlert();
      }
    }, error =>{
      console.log(error);
    });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Notificación',
      subHeader: 'Tickets',
      message: 'No tiene tickets asignados',
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
  buscar(event:{ detail:{value: any; }; }){
    this.textoBusqueda = event.detail.value;
  }
  detalle(ticket:any){
    localStorage.setItem('id',ticket.codticket)
    this.navCtrl.navigateForward('detalle-ticket-tecnico');
  }

  async filtroButton() {
    const alert = await this.alertController.create({
      header: 'Configuracion de barra de busqueda',
      subHeader: 'Seleccione el modo de busqueda:',
      inputs: [
        {
          type: 'radio',
          label: 'Tipo de Servicio',
          value: 'TipoServicio'
        },
        {
          type: 'radio',
          label: 'Nivel de prioridad',
          value: 'prioridad'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: (data: any) => {

          }
        },
        {
          text: 'Listo',
          handler: (data: any) => {
            if(data=="TipoServicio"){
              this.tipoServicio='TipoServicio';
              this.defectSelect="Buscar por: Tipo de Servicio";
            }
            if(data=="prioridad"){
              this.tipoServicio='prioridad';
              this.defectSelect="Buscar por: Nivel de prioridad";
            }
          }
        }
      ]
    });
    await alert.present();
  }

}
