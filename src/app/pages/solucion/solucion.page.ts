import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

import { Solucion } from './../../models/solucion';

import { SolucionService } from './../../services/solucion.service';
import { TicketService } from 'src/app/services/ticket.service';
import { UtilsService } from './../../utils/utils.service';
import { HistorialEscalarService } from 'src/app/services/historial-escalar.service';

@Component({
  selector: 'app-solucion',
  templateUrl: './solucion.page.html',
  styleUrls: ['./solucion.page.scss'],
})
export class SolucionPage implements OnInit {
  solucion:Solucion = new Solucion();
  ticket=[];
  usuario=[];
  tecnico:any;
  historial:any;
  email:any;
  id = localStorage.getItem('id');
  constructor(private modalController:ModalController,
      private serviticket:TicketService,
      public alertController: AlertController,
      private toastService: UtilsService,
      private service:SolucionService,
      private emailComposer:EmailComposer,
      private serviHistorial: HistorialEscalarService) { }

  ngOnInit() {
    this.getTicketById(this.id);
  }


  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }
  guardar(){
    this.solucion.codSolucion = Number(this.id);
    let fecha = new Date();
    let fehoy = "";
    if ((fecha.getMonth()+1)<10) {
      fehoy = fecha.getFullYear()+'-0'+(fecha.getMonth()+1)+'-'+fecha.getDate();
    }else{
      fehoy = fecha.getFullYear()+'-'+(fecha.getMonth()+1)+'-'+fecha.getDate();
    }
    this.solucion.fechafinalizacion = fehoy;
    this.solucion.ticket = this.ticket;
    console.log(this.solucion);
    this.service.setSolucion(this.solucion).then(
      data =>{
        this.dismiss();
        this.confirmar(this.id);
        this.notificarUsuario(this.email, this.id);
      //  this.notificarTecnico();
      },err =>{
        console.log(err);
      }
    );
  }

  cambiarEstado(cod_estado:number, id:any, res1:any){
    this.serviticket.putCambiarEstado(cod_estado,id).then(
      data =>{
        this.toastService.presentToastLenin(res1)
      },err =>{
        this.toastService.presentToastLenin("A ocurrido un problema")
      }
    );
  }
  confirmar(codticket:any){
    this.serviticket.putConfirmar(codticket).then((data)=>{
      this.toastService.presentToastLenin("Enviado Confimación al usuario");
    },err =>{
      this.toastService.presentToastLenin("Error");
    });
  }

  getTicketById(id:any){
    this.serviticket.getTicketById(id).then((data)=>{
      this.ticket = data[0];
      this.usuario = data[0].usuario;
      this.email = data[0].usuario.email;
      this.tecnico = data[0].tecnico.nivel;
    });
  }
  notificarUsuario(gmailUsuario:string, ticket:any){
    this.emailComposer.addAlias('gmail', 'com.google.android.gm')
    this.emailComposer.open({
      app: 'gmail',
      to: gmailUsuario,
      subject: 'SOLUCION DEl TICKET',
      body: 'Es un grato placer hacerle llegar mis mas cordiales saludos; así mismo darle a conocer que ha sido asignada la solucion del ticket con codigo: '+ticket
      +"Por favor inicie sesión desde nuestra app o pagina web para que conosca la resolución de la misma",
      isHtml: true
    });
    this.toastService.presentToastLenin("Gmail Usuario");
  }

  notificarTecnico(){
    if (this.tecnico != 1) {
      this.llamarhistorial(this.id)
    }
  }

  llamarhistorial(id:any){
    this.serviHistorial.getById(id).then((data) => {
      if (data != null) {
          this.historial = data[0].nivel1.email;
          this.notificaTecnico(this.historial,this.id);
          this.toastService.presentToastLenin("Enviando solución al Tecnico de Nivel 1");
      }
      else{
        this.toastService.presentToastLenin("Algo a salido mal");
      }
    },err =>{
      console.log(err);
    });
  }

  notificaTecnico(gmail:string, ticket:any){
    this.emailComposer.addAlias('gmail', 'com.google.android.gm')
    this.emailComposer.open({
      app: 'gmail',
      to: gmail,
      subject: 'SOLUCION DEl TICKET',
      body: 'Es un grato placer hacerle llegar mis mas cordiales saludos; así mismo darle a conocer que ha sido asignada la solucion del ticket con codigo: '+ticket
      +"Y esta a la espera de la confirmación del usuario",
      isHtml: true
    });
  }

}
