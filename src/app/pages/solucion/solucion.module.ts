import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SolucionPageRoutingModule } from './solucion-routing.module';

import { SolucionPage } from './solucion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SolucionPageRoutingModule
  ],
  declarations: [SolucionPage]
})
export class SolucionPageModule {}
