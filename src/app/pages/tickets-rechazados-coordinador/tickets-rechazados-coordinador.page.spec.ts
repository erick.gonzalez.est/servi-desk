import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TicketsRechazadosCoordinadorPage } from './tickets-rechazados-coordinador.page';

describe('TicketsRechazadosCoordinadorPage', () => {
  let component: TicketsRechazadosCoordinadorPage;
  let fixture: ComponentFixture<TicketsRechazadosCoordinadorPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketsRechazadosCoordinadorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TicketsRechazadosCoordinadorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
