import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TicketsRechazadosCoordinadorPage } from './tickets-rechazados-coordinador.page';

const routes: Routes = [
  {
    path: '',
    component: TicketsRechazadosCoordinadorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TicketsRechazadosCoordinadorPageRoutingModule {}
