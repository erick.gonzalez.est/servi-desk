import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { TicketService } from 'src/app/services/ticket.service';
import { Component, OnInit } from '@angular/core';
import { UtilsService } from 'src/app/utils/utils.service';

@Component({
  selector: 'app-tickets-rechazados-coordinador',
  templateUrl: './tickets-rechazados-coordinador.page.html',
  styleUrls: ['./tickets-rechazados-coordinador.page.scss'],
})
export class TicketsRechazadosCoordinadorPage implements OnInit {
  rechazados:any=[];
  estadoTicket:string;
  ticket:any=[];
  textoBusqueda:string='';
  tipoServicio:string='Servicio';
  defectSelect:string="Buscar por: Servicio";
  constructor(private loadingController:LoadingController,private utils:UtilsService,private servicioTicket:TicketService,private alertController:AlertController,private navCtrl: NavController) { 
    this.estadoTicket="Incidentes";
  }
  doRefresh(event) {
    
    this.ngOnInit();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 500);
  }
  async descargandoDatos(){
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      message: 'Descargando datos...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    setTimeout(() => {
      this.ConsultaDeRickets();
    }, 1000);
    setTimeout(() => {
      loading.dismiss();
    }, 1000);
  }
  ngOnInit() {
    this.descargandoDatos();
  }
  buscar(event){
    this.textoBusqueda=event.detail.value;
  }
  ConsultaDeRickets(){
    this.servicioTicket.getTicket().then(data=>{
      if(data!=null){
        this.ticket=data;
        this.rechazados=this.ticket.sort(function(a, b) { a = new Date(a.fechaCreacion); b = new Date(b.fechaCreacion); return a>b ? -1 : a<b ? 1 : 0; });
      }else{
        this.utils.presentToastLenin("No se encontraron datos.");
      }
      
      console.log(data);
    })
  }
  AbrirTicket(ticket:any){
    this.navCtrl.navigateForward('/detalle-ticket-coordinador/'+ticket.codticket);
    console.log(ticket )
  }
  segmentChanged(ev: any) {
    this.estadoTicket=ev.detail.value;
  }
  async filtroButton() {
    const alert = await this.alertController.create({
      header: 'Configuracion de barra de busqueda',
      subHeader: 'Seleccione el modo de busqueda:',
      inputs: [
        {
          type: 'radio',
          label: 'Nombre de usuario',
          value: 'usuario'
        },
        {
          type: 'radio',
          label: 'Tecnico',
          value: 'tecnico'
        },
        {
          type: 'radio',
          label: 'Nivel de Criticidad',
          value: 'criticidad'
        },
        {
          type: 'radio',
          label: 'Servicio',
          value: 'Servicio'
        },
        {
          type: 'radio',
          label: 'Tipo de Severidad',
          value: 'severidad'
        },
        {
          type: 'radio',
          label: 'Nivel de prioridad',
          value: 'prioridad'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: (data: any) => {
            
          }
        },
        {
          text: 'Ok',
          handler: (data: any) => {
            
            if(data=="usuario"){
              this.tipoServicio="usuario";
              this.defectSelect="Buscar por: Nombre de usuario";
            }
            if(data=="tecnico"){
              this.tipoServicio='tecnico';
              this.defectSelect="Buscar por: tecnico";
            } 
            if(data=="TipoServicio"){
              this.tipoServicio='TipoServicio';
              this.defectSelect="Buscar por: Tipo de Servicio";
            }
            if(data=="criticidad"){
              this.tipoServicio='criticidad';
              this.defectSelect="Buscar por: Nivel de criticidad";
            }
            if(data=="Servicio"){
              this.tipoServicio='Servicio';
              this.defectSelect="Buscar por: Servicio";
            }
            if(data=="severidad"){
              this.tipoServicio='severidad';
              this.defectSelect="Buscar por: Nivel de Severidad";
            }
            if(data=="prioridad"){
              this.tipoServicio='prioridad';
              this.defectSelect="Buscar por: Nivel de prioridad";
            }
          }
        }
      ]
    });
    await alert.present();
  }
}
