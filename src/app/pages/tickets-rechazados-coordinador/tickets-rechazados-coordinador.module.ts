import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TicketsRechazadosCoordinadorPageRoutingModule } from './tickets-rechazados-coordinador-routing.module';

import { TicketsRechazadosCoordinadorPage } from './tickets-rechazados-coordinador.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    TicketsRechazadosCoordinadorPageRoutingModule
  ],
  declarations: [TicketsRechazadosCoordinadorPage]
})
export class TicketsRechazadosCoordinadorPageModule {}
