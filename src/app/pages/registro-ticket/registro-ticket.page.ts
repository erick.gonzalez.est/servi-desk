import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChooserResult } from '@ionic-native/chooser/ngx';
import { NavController } from '@ionic/angular';
import { Catalogo } from 'src/app/models/catalogos';
import { Servicio } from 'src/app/models/servicio';
import { Severidad } from 'src/app/models/severidad';
import { Ticket } from 'src/app/models/ticket';
import { ViaComunicacion } from 'src/app/models/via-comunicacion';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { ConsumoImagenService } from 'src/app/services/consumo-imagen.service';
import { ServicioService } from 'src/app/services/servicio.service';
import { SeveridadService } from 'src/app/services/severidad.service';
import { TicketService } from 'src/app/services/ticket.service';
import { ViaComunicacionService } from 'src/app/services/via-comunicacion.service';
import { UtilsService } from 'src/app/utils/utils.service';
import { DatabaseService } from 'src/app/services/database.service';

@Component({
  selector: 'app-registro-ticket',
  templateUrl: './registro-ticket.page.html',
  styleUrls: ['./registro-ticket.page.scss'],
})
export class RegistroTicketPage implements OnInit {
  fileObj: ChooserResult;
  uploadText: any;
  downloadText: any;
  dataServicio: Servicio[] = [];
  dataCatalogo: Catalogo[] = [];
  dataSeveridad: Severidad[] = [];
  dataViaComunicacion: ViaComunicacion[] = [];
  codUsuario: any;
  fecha: string = new Date().toISOString();
  sendMsg: any = '';
  urlChange: any;
  tickets: any = [];

  // from lista tickets
  titulo: string;
  codTipo: any;

  //ng Variables
  codCatalogo: any;
  tituloCatalogo: any;
  codServicio: any;
  tituloServicio: any;
  codSeveridad: any;
  nivelSeveridad: any;
  descripcionTicket: any;
  codViaComunicacion: any;
  nombreViaComunicacion: any;
  imgUrl: any;
  codTicket: any;
  editable: any;
  fileName: any;

  //datos para clacular el SLA
  valorCritividad: any;
  valorPrioridad: any;
  valorSevridad: any;

  archivo: File;
  photoSelected: string | ArrayBuffer;
  processing: boolean;
  uploadImage: string;
  img: String;

  constructor(
    private utils: UtilsService,
    private consumo: ConsumoImagenService,
    private servicioService: ServicioService,
    private catalogoService: CatalogoService,
    private severidadService: SeveridadService,
    private viaComunicacionService: ViaComunicacionService,
    private ticketService: TicketService,
    private router: Router,
    public navCtrl: NavController,
    private db: DatabaseService
  ) {}

  async ngOnInit() {
    this.getCatalogos();
    this.getSeveridad();
    this.getViaComunicacion();
    this.codTipo = localStorage.getItem('codTipo');
    this.titulo = localStorage.getItem('tituloTipo');
    console.log(
      ' al iniciar se traen estos datos: ' + this.codTipo,
      this.titulo
    );

    this.editable = localStorage.getItem('editable');
    console.log('el registro es editable? ' + this.editable);
    if (this.editable === '1') {
      this.codTicket = localStorage.getItem('codTicket');
      console.log('existe un codigo? ' + this.codTicket);
      this.getTicketById(this.codTicket);
      // this.getServicios(this.codTipo, this.codCatalogo);
    }
    this.codUsuario = parseInt(localStorage.getItem('idUsuario'));

    this.db.getDatabaseState().subscribe((rdy) => {
      if (rdy) {
        this.db.getTickets().subscribe((data) => {
          this.tickets = data;
          console.log('Desde registros ts' + this.tickets);
        });
      }
    });
  }

  ionViewDidLeave() {
    localStorage.removeItem('codCatalogo');
    localStorage.removeItem('tituloCatalogo');
    localStorage.removeItem('codServicio');
    localStorage.removeItem('tituloServicio');
    localStorage.removeItem('codSeveridad');
    localStorage.removeItem('nivelSeveridad');
    localStorage.removeItem('codSeveridad');
    localStorage.removeItem('nivelSeveridad');
    localStorage.removeItem('codViaComunicacion');
    localStorage.removeItem('nombreViaComunicacion');
    localStorage.removeItem('url');
    localStorage.removeItem('codTipo');
    localStorage.removeItem('tituloTipo');
    localStorage.removeItem('ticket');
    console.log('si esta borrando cosas :D');
  }

  getTicketById(codTicket: any) {
    this.ticketService.getTicketById(codTicket).then(async (data) => {
      this.codCatalogo = data[0].servicio.catalogo.codCatalogo;
      this.tituloCatalogo = data[0].servicio.catalogo.categoria;
      this.codServicio = data[0].servicio.codServicio;
      this.tituloServicio = data[0].servicio.titulo;
      this.codSeveridad = data[0].severidad.codSeveridad;
      this.nivelSeveridad = data[0].severidad.nivelSeveridad;
      this.descripcionTicket = data[0].descripcionTicket;
      this.codViaComunicacion = data[0].viaComunicacion.codViaComunicacion;
      this.nombreViaComunicacion = data[0].viaComunicacion.nombre;
      this.imgUrl = data[0].url;
      this.codTipo = data[0].servicio.tipo.codTipo;
      this.titulo = data[0].servicio.tipo.nombre;
      // this.getServicios(this.codTipo, this.codCatalogo);
      await this.getValoresServicios(this.codServicio);
    });
  }

  popView() {
    this.router.navigateByUrl('/lista-ticket-usuario');
  }

  popViewMod() {
    this.router.navigateByUrl('/detalle-ticket-usuario');
  }

  async uploadFile() {
    this.consumo.onClickSubir();
    setTimeout(async () => {
      this.imgUrl = localStorage.getItem('url');
      console.log('url de imagen ' + this.imgUrl);
    }, 3000);
  }

  onChange(fileChangeEvent: { target: { files: File[] } }) {
    this.archivo = fileChangeEvent.target.files[0];
    this.imgUrl = '';
    this.fileName = this.archivo.name;
    console.log(this.fileName);
    this.consumo.onChange(this.archivo);
    this.urlChange = 1;
  }

  async uploadTicket() {
    console.log("cod servicio "+ this.codServicio);
    if (this.codServicio != null && this.codCatalogo != null && this.codSeveridad != null && this.codViaComunicacion != null && this.descripcionTicket != null) {
      await this.uploadFile();
      this.utils.presentAlertTicket(
        'No agregó ningun archivo a su solicitud'
      );
      setTimeout(async () => {
        await this.createTicket();
        this.utils.presentToastLenin(this.sendMsg);
        this.popView();
      }, 3500);
      localStorage.removeItem('url'); 
    }else {
      this.utils.presentAlert(
        'Porfavor, ingrese los campos necesarios'
      );
    }
  }

  async modificarTicket() {
    if (this.codServicio != null && this.descripcionTicket != null) {
    console.log('test antes de if modificar: ', this.imgUrl);
    if (this.urlChange != null) {
      console.log('test despues de if modificar: ', this.imgUrl);
      await this.uploadFile();
    }
    this.utils.presentLoading('Guardando');
    setTimeout(async () => {
      await this.createTicket();
      this.utils.presentToastLenin(this.sendMsg);
      this.popViewMod();
    }, 3500);
    localStorage.removeItem('url');
  }else {
    this.utils.presentAlert(
      'Porfavor, ingrese los campos necesarios'
    );
  }
  }

  async onChangeCatalogo(event: { target: { value: any } }) {
    this.tituloServicio = null;
    this.codServicio = null;
    localStorage.removeItem('tituloServicio');
    localStorage.removeItem('codServicio');
    localStorage.setItem('codCatalogo', event.target.value);
    let codCatalogo = localStorage.getItem('codCatalogo');
    this.getServicios(this.codTipo, codCatalogo);
    console.log('codigo de catalogo seleccionado/guardado ' + codCatalogo);
    for (const data of this.dataCatalogo) {
      if (codCatalogo === data.codCatalogo.toString()) {
        localStorage.setItem('tituloCatalogo', data.categoria);
        this.tituloCatalogo = localStorage.getItem('tituloCatalogo');
      } else {
        this.tituloCatalogo = data.categoria;
      }
    }
  }

  async onChangeServicio(event: { target: { value: any } }) {
    localStorage.setItem('codServicio', event.target.value);
    let codServicio = localStorage.getItem('codServicio');
    console.log('codigo de servicio seleccionado/guardado ' + codServicio);
    for (const data of this.dataServicio) {
      if (codServicio === data.codServicio.toString()) {
        localStorage.setItem('tituloServicio', data.titulo);
        this.tituloServicio = localStorage.getItem('tituloServicio');
      } else {
        this.tituloServicio = data.titulo;
      }
    }
  }

  async onChangeSeveridad(event: { target: { value: any } }) {
    let vseveridad;
    localStorage.setItem('codSeveridad', event.target.value);
    let codSeveridad = localStorage.getItem('codSeveridad');
    console.log('codigo de severidad seleccionado/guardado ' + codSeveridad);
    this.severidadService
    .getSeveridadPorCodSeveridad(codSeveridad)
    .then((data) => {
      console.log(data);
      vseveridad = data;
      this.valorSevridad = vseveridad.valorSeveridad;
      console.log(
        this.valorCritividad,
        ' , ',
        this.valorPrioridad,
        ' , ',
        this.valorSevridad
      );
    });
    for (const data of this.dataSeveridad) {
      if (codSeveridad === data.codSeveridad.toString()) {
        localStorage.setItem('nivelSeveridad', data.nivelSeveridad);
        this.nivelSeveridad = localStorage.getItem('nivelSeveridad');
      } else {
        this.nivelSeveridad = data.nivelSeveridad;
      }
    }
  }

  async onChangeViaComunicacion(event: { target: { value: any } }) {
    localStorage.setItem('codViaComunicacion', event.target.value);
    let codViaComunicacion = localStorage.getItem('codViaComunicacion');
    console.log(
      'codigo de viaComunicacion seleccionado/guardado ' + codViaComunicacion
    );
    for (const data of this.dataViaComunicacion) {
      if (codViaComunicacion === data.codViaComunicacion.toString()) {
        localStorage.setItem('nombreViaComunicacion', data.nombre);
        this.nombreViaComunicacion = localStorage.getItem(
          'nombreViaComunicacion'
        );
      } else {
        this.nombreViaComunicacion = data.nombre;
      }
    }
  }

  /////////////////////////////////////////////////////////////

  getViaComunicacion() {
    this.viaComunicacionService.getViaComunicacion().then((data: any) => {
      this.dataViaComunicacion = data;
    });
  }

  getSeveridad() {
    this.severidadService.getListSeveridad().then((data: any) => {
      this.dataSeveridad = data;
    });
  }

  getCatalogos() {
    this.catalogoService.getCatalogo().then((data: any) => {
      this.dataCatalogo = data;
    });
  }

  getServicios(tipo: any, catalogo: any) {
    this.servicioService.getServiciosByTipo(tipo, catalogo).then(
      (data: any) => {
        this.dataServicio = data;
        console.log('cantidad de servicios: ' + data.length);
        if (this.dataServicio.length == 0) {
          this.utils.presentAlert(
            'No existen servicios disponibles para esta categoria'
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  async getValoresServicios(codServicio: any){
    let values: any;
    let vseveridad: any;
    await this.severidadService
    .getSeveridadPorCodSeveridad(this.codSeveridad)
    .then((data) => {
      console.log(data);
      vseveridad = data;
      this.valorSevridad = vseveridad.valorSeveridad;
    });
    await this.servicioService
    .getServicioByCodigo(codServicio)
    .then((data) => {
      console.log(data);
      values = data;
      this.valorCritividad = values.criticidad.valor;
      this.valorPrioridad = values.prioridad.tiempoRespuesta;
      console.log(
        this.valorCritividad,
        ' , ',
        this.valorPrioridad,
        ' , ',
        this.valorSevridad
      );
    });
  }

  async createTicket() {
    let dataTicket: Ticket;
    let ticketStatus: any;
    let tiempoRespuestaSLA: number;
    tiempoRespuestaSLA = Math.trunc(
      this.valorCritividad * this.valorPrioridad * this.valorSevridad
    );

    dataTicket = {
      codTicket: this.codTicket,
      codCatalogo: this.codCatalogo,
      codServicio: this.codServicio,
      descripcionTicket: this.descripcionTicket,
      codSeveridad: this.codSeveridad,
      codViaComunicacion: this.codViaComunicacion,
      codUsuario: this.codUsuario,
      sla: tiempoRespuestaSLA,
      url: this.imgUrl,
      fechaCreacion: this.fecha,
      estado: 1,
    };

    console.log(dataTicket);

    try {
      if (this.editable === '1') {
        ticketStatus = (await this.ticketService.updateTicket(dataTicket))
          .status;
      } else {
        ticketStatus = (await this.ticketService.createTicket(dataTicket))
          .status;

        try {
          console.log(
            'datos de ticket guardar en bd ' + JSON.stringify(dataTicket)
          );
          await this.db.addTickets(dataTicket).then((_) => {
            dataTicket = null;
          });
        } catch (err) {
          console.log('Registrar Error ' + err.error);
        }
      }
      console.log(ticketStatus + ' ticket status');
      if (ticketStatus === 200 || ticketStatus === 201) {
        if (this.editable === '1') {
          this.sendMsg = 'Ticket modificado exitosamente';
        } else {
          this.sendMsg = 'Ticket creado exitosamente';
        }
      } else {
        if (this.editable === '1') {
          this.sendMsg = 'Hubo un problema al tratar de moficar el ticket';
        } else {
          this.sendMsg = 'Hubo un problema al tratar de crear el ticket';
        }
      }
    } catch (error) {
      this.sendMsg = 'Error al tratar de crear o modificar el ticket ';
      console.log(
        JSON.stringify(error) + ' error al crear o moficar el ticket'
      );
    }
  }
}
