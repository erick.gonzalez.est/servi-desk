import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistroTicketPage } from './registro-ticket.page';

const routes: Routes = [
  {
    path: '',
    component: RegistroTicketPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistroTicketPageRoutingModule {}
