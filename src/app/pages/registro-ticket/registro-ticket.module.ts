import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistroTicketPageRoutingModule } from './registro-ticket-routing.module';

import { RegistroTicketPage } from './registro-ticket.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistroTicketPageRoutingModule
  ],
  declarations: [RegistroTicketPage]
})
export class RegistroTicketPageModule {}
