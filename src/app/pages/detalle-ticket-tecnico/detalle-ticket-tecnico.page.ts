import { Component, OnInit } from '@angular/core';
import { TicketService } from 'src/app/services/ticket.service';
import { TecnicoService } from 'src/app/services/tecnico.service';
import { AlertController, ModalController } from '@ionic/angular';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

import { SolucionPage } from "./../solucion/solucion.page";
import { UtilsService } from './../../utils/utils.service';
import { NavController } from '@ionic/angular';
import { SolucionService } from './../../services/solucion.service';
import { ListaTecnicosCoordinarAsignarPage } from './../lista-tecnicos-coordinar-asignar/lista-tecnicos-coordinar-asignar.page';
import { HistorialEscalarService } from 'src/app/services/historial-escalar.service';
import { Historial } from 'src/app/models/historial';

@Component({
  selector: 'app-detalle-ticket-tecnico',
  templateUrl: './detalle-ticket-tecnico.page.html',
  styleUrls: ['./detalle-ticket-tecnico.page.scss'],
})
export class DetalleTicketTecnicoPage implements OnInit {
  id:any;
  nivel2:any=[];
  ticket:any=[];
  servicios:any=[];
  catalogos:any=[];
  personas:any=[];
  usuarios:any=[];
  prioridades:any=[];
  listPrioridades:any=[];
  viaComunicacion:any=[];
  tecnico:any=[];
  estado:any=[];
  solucion:any =[];
  nombreTecnico:any = [];
  codCoordinador = 1;
  idTecnico:number;
  TecnicoAnotificar:any=[];
  historial:Historial = new Historial();
  constructor(private servicioTicket:TicketService,
    private toastService: UtilsService,
    private serviTecnico:TecnicoService,
    public alertController: AlertController,
    private serviSolucion:SolucionService,
    private modalCtrl:ModalController,
    private emailComposer:EmailComposer,
    private nav:NavController,
    private serviHistorial:HistorialEscalarService) { }

  ngOnInit() {
    this.id = localStorage.getItem('id');
    console.log(this.id);

    this.getTicketById(this.id);
    this.mostrarSolución(this.id);
  }


  getTicketById(id:number) {
    this.servicioTicket.getTicketById(id)
    .then(data => {
      this.ticket = data[0];
      this.servicios=data[0].servicio;
      this.catalogos=data[0].servicio.catalogo;
      this.personas=data[0].usuario.persona;
      this.usuarios=data[0].usuario;
      this.prioridades=data[0].servicio.prioridad;
      this.tecnico=data[0].tecnico;
      this.viaComunicacion=data[0].viaComunicacion;
      this.estado=data[0].estado;
    });
  }
  cerrar(){
    localStorage.removeItem('id');
  }
  async presentAlertaVerDescripcionServicio() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Detalle del Servicio',
      subHeader: 'Descripcion:',
      message: this.servicios.descripcion,
      buttons: ['OK']
    });

    await alert.present();
  }
  async presentAlertaVerDescripcionTicket() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Detalle del Ticket',
      subHeader: 'Descripcion:',
      message: this.ticket.descripcionTicket,
      buttons: ['OK']
    });

    await alert.present();
  }
  async abrirModalSolucion(){
    const modal = await this.modalCtrl.create({
      component:SolucionPage,
      swipeToClose: true,
    });
    await modal.present();
  }
  async presentAlertaCerrarSinSolucion() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Aleta',
      subHeader: 'Cerrar Ticket',
      message: 'Seguro de cerrar el ticket sin solución',
      buttons: [{
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'OK',
          handler: () => {
            this.cambiarEstado(4,this.id,"Cerrado sin solución","Error al cerrar el ticket");
          }
        }]
    });
      await alert.present();
    }
  async presentAlertaRechazar() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Aleta',
      subHeader: 'Reasignar',
      message: 'Seguro de reasignar el ticket',
      buttons: [{
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'OK',
          handler: () => {
            this.cambiarEstado(1,this.id,"Ticket Reasignado","Error al reasignar el ticket");
            this.reasignar(this.id);
            this.nav.navigateForward('lista-tecnico');
          }
        }]
    });
      await alert.present();
    }
  cambiarEstado(cod_estado:number, id:any, txt1:any, txt2:any){
      this.servicioTicket.putCambiarEstado(cod_estado,id).then(
        data =>{
          this.toastService.presentToastLenin(txt1)
        },err =>{
          this.toastService.presentToastLenin(txt2)
        }
      );
    }
  reasignar(codticket:any){
      this.servicioTicket.putReasignar(codticket).then(
        data =>{
          this.toastService.presentToastLenin("Reasignado con exito");
        },err =>{
          this.toastService.presentToastLenin("Error");
        }
      );
    }

  mostrarSolución(cod_solucion:any){
      this.serviSolucion.getSolucionById(cod_solucion).then(
        data => {
          this.solucion = data[0];
          console.log(this.solucion);
        },err =>{
          console.log(err);
        }
      );
    }
  async presentAlertaVerSolucion() {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Solución Ticket',
        subHeader: 'Descripcion:',
        message: this.solucion.descripcion,
        buttons: ['OK']
      });

      await alert.present();
    }
  async abrirModalListTecnicos() {
    let nivel = '2';
    localStorage.setItem('nivel', nivel);
    const modal = await this.modalCtrl.create({
      component: ListaTecnicosCoordinarAsignarPage,
      swipeToClose: true,
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data.tecnico != undefined) {
      this.nombreTecnico = [];
      this.nombreTecnico = data.tecnico;
      this.serviTecnico
        .GetTecnicoIdForCedula(this.nombreTecnico.cedula)
        .then((data) => {
          this.idTecnico=Number(data);
          this.servicioTicket.putAsignarTecnico(Number(data),this.codCoordinador,this.ticket.codticket).then((data)=>{
            this.toastService.presentToastLenin("Ticket reescalado de manera correcta")
            this.servicioTicket.putFechaAsignacion(this.ticket.codticket,this.ticket).then((data)=>{
              console.log("Fecha establecida");
            //  this.guardarHistorial()
            })
            this.serviTecnico.getTecnicoForId(this.idTecnico).then((data)=>{
                this.TecnicoAnotificar=data;
                this.notificarTecnico(this.TecnicoAnotificar.email);
            })
          }).catch((data)=> {
            this.toastService.presentToastLenin("Error al asignar tecnico")
          })
        });
    }
  }
  notificarTecnico(gmailTecnico:string){
      this.emailComposer.addAlias('gmail', 'com.google.android.gm')
      this.emailComposer.open({
        app: 'gmail',
        to: gmailTecnico,
        subject: 'ASIGNACION DE TICKET',
        body: '<p>Es un grato placer hacerle llegar mis mas cordiales saludos; así mismo darle a conocer que ha sido reescalado a la resolucion del ticket con codigo: '+this.ticket.codticket
        +"</p><br/> <p>Emitido por: "+this.usuarios.nombres+ " </p> <br/> <p>Prioridad "+this.prioridades.nivelPrioridad + "</p><br/> <p> Con HORA Y FECHA: "+this.ticket.fechaCreacion+ ". </p> <br/><p>Esta Asignación fue creada con fecha: "+ this.ticket.fechaAsignacion+ ".</p><br/><p> Saludos.</p>",
        isHtml: true
      });
      this.toastService.presentToastLenin("Abrir Gmail");
    }

  guardarHistorial(){
    let fecha = new Date();
    let fehoy = "";
    if ((fecha.getMonth()+1)<10) {
      fehoy = fecha.getFullYear()+'-0'+(fecha.getMonth()+1)+'-'+fecha.getDate();
    }else{
      fehoy = fecha.getFullYear()+'-'+(fecha.getMonth()+1)+'-'+fecha.getDate();
    }
    //this.historial.codHistorial = 1;
    this.historial.fechaEscalar = fehoy;
    this.historial.nivel1 = this.tecnico;
    this.serviTecnico.getTecnicoForId(this.idTecnico).then((data)=>{
      this.historial.nivel2 = data;
    },err =>{
      console.log(err);
    });
    this.historial.ticket = this.ticket;
    console.log(this.historial);
    this.serviHistorial.setHistorial(this.historial).then((data)=>{
      console.log(this.historial);
      console.log(data);
    },err =>{
      console.log(err);
      console.log(this.historial);
    });
  }
}
