import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalleTicketTecnicoPage } from './detalle-ticket-tecnico.page';

const routes: Routes = [
  {
    path: '',
    component: DetalleTicketTecnicoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleTicketTecnicoPageRoutingModule {}
