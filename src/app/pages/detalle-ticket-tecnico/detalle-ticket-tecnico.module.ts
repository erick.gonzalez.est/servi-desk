import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleTicketTecnicoPageRoutingModule } from './detalle-ticket-tecnico-routing.module';

import { DetalleTicketTecnicoPage } from './detalle-ticket-tecnico.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleTicketTecnicoPageRoutingModule
  ],
  declarations: [DetalleTicketTecnicoPage]
})
export class DetalleTicketTecnicoPageModule {}
