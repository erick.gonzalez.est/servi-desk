import { PipesModule } from 'src/app/pipes/pipes.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListaTicketUsuarioPageRoutingModule } from './lista-ticket-usuario-routing.module';

import { ListaTicketUsuarioPage } from './lista-ticket-usuario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    ListaTicketUsuarioPageRoutingModule
  ],
  declarations: [ListaTicketUsuarioPage]
})
export class ListaTicketUsuarioPageModule {}
