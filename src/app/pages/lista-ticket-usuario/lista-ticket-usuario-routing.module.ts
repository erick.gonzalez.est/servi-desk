import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaTicketUsuarioPage } from './lista-ticket-usuario.page';

const routes: Routes = [
  {
    path: '',
    component: ListaTicketUsuarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaTicketUsuarioPageRoutingModule {}
