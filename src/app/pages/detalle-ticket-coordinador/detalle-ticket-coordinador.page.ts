import { SolucionService } from './../../services/solucion.service';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { SeveridadService } from './../../services/severidad.service';
import { UtilsService } from './../../utils/utils.service';
import { TecnicoService } from './../../services/tecnico.service';

import { ListaTecnicosCoordinarAsignarPage } from './../lista-tecnicos-coordinar-asignar/lista-tecnicos-coordinar-asignar.page';
import { PrioridadService } from './../../services/prioridad.service';
import { AlertController, ModalController, LoadingController } from '@ionic/angular';
import { TicketService } from 'src/app/services/ticket.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-detalle-ticket-coordinador',
  templateUrl: './detalle-ticket-coordinador.page.html',
  styleUrls: ['./detalle-ticket-coordinador.page.scss'],
})
export class DetalleTicketCoordinadorPage implements OnInit {
  ticket: any = [];
  servicios: any = [];
  catalogos: any = [];
  personas: any = [];
  prioridades: any = [];
  severidadList: any = [];
  usuarios: any = [];
  nombreTecnico: any = [];
  criticidad:any=[];
  severidad:any=[];
  severidad1:any=[];
  nivelSeveridad:string;
  idTecnico:number;
  TecnicoAnotificar:any=[];
  estadoticket:string;
  codCoordinador:number;
  estado:any=[];
  solucionticket:any=[];
  tecnicoEscala:any=[];
  viaComunicacion:any=[];
  constructor(
    private servicioTecnico: TecnicoService,
    private servicioTicket: TicketService,
    private route: ActivatedRoute,
    public alertController: AlertController,
    private modalCtrl: ModalController,
    private toastService: UtilsService,
    private severidadService:SeveridadService,
    private emailComposer:EmailComposer,
    private solucionService:SolucionService,
    private loadingController:LoadingController
  ) {}
  id: number;
  ngOnInit() {
    this.descargandoDatos();
  }
  cargarDatos(){
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.getTicketById(this.id);
    this.getObetenerListSeveridad();
    this.codCoordinador= Number(window.localStorage["idUsuario"])
    console.log(window.localStorage["idUsuario"]);
    console.log(window.localStorage["TipoUsuario"])
  }
  async descargandoDatos(){
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      message: 'Descargando datos...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    setTimeout(() => {
      this.cargarDatos();
    }, 1000);
    setTimeout(() => {
      loading.dismiss();
    }, 1000);
  }
  async abrirModalListTecnicos() {
    let nivel = '1';
    localStorage.setItem('nivel', nivel);
    const modal = await this.modalCtrl.create({
      component: ListaTecnicosCoordinarAsignarPage,
      swipeToClose: true,
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data.tecnico != undefined) {
      this.nombreTecnico = [];
      this.nombreTecnico = data.tecnico;
      this.servicioTecnico
        .GetTecnicoIdForCedula(this.nombreTecnico.cedula)
        .then((data) => {
          this.idTecnico=Number(data);
          this.servicioTicket.putAsignarTecnico(Number(data),this.codCoordinador,this.ticket.codticket).then((data)=>{
            this.toastService.presentToastLenin("Tecnico Asignado Correctamente")
            this.servicioTicket.putFechaAsignacion(this.ticket.codticket,this.ticket).then((data)=>{
              console.log("Fecha establecida");
            })
            this.servicioTecnico.getTecnicoForId(this.idTecnico).then((data)=>{
                this.TecnicoAnotificar=data;
                this.cambiarEstado(this.ticket.codticket);
                this.notificarTecnico(this.TecnicoAnotificar.email);
            })
          }).catch((data)=> {
            this.toastService.presentToastLenin("Error al asignar tecnico")
          })
        });
    }
  }
  notificarTecnico(gmailTecnico:string){
    this.emailComposer.addAlias('gmail', 'com.google.android.gm')
    this.emailComposer.open({
      app: 'gmail',
      to: gmailTecnico,
      subject: 'ASIGNACION DE TICKET',
      body: '<p>Es un grato placer hacerle llegar mis mas cordiales saludos; así mismo darle a conocer que he sido asignado a la resolucion del ticket con codigo: '+this.ticket.codticket 
      +"</p> <br/> <p>Emitido por: "+this.usuarios.persona.nombres+ " </p>  <br/>  <p>Prioridad: "+this.prioridades.nivelPrioridad + "</p>  <br/> <p>Con HORA Y FECHA: "+this.ticket.fechaCreacion+ ". </p> <br/> <p>Esta Asignación fue creada con fecha: "+ this.ticket.fechaAsignacion+ ". </p> </br> <p>Saludos.</p>",
      isHtml: true
    });
  }
  getObetenerListSeveridad() {
    this.severidadService.getListSeveridad().then((data)=>{
      console.log(data);
      this.severidadList=data;
    }).catch((data)=>{
      this.toastService.presentToastLenin("Error al obtener datos")
    })
  }
  putCambiarSeveridad(text_severidad: string) {
    //cambiar
    this.severidadService.getSeveridadPorNivelSeveridad(text_severidad).then((data) => {
      console.log(data)
      this.severidad1=data;
      this.nivelSeveridad=this.severidad1.nivelSeveridad;
      this.severidadService.putSeveridadTicket(this.severidad1.codSeveridad,this.ticket.codticket).then((data) => {
        this.toastService.presentToastLenin("Severidad Cambiada")
      }).catch((data)=>{
        this.toastService.presentToastLenin("Error al cambiar la severidad")
      })
    });

  }
  getTicketById(id: number) {
    this.servicioTicket.getTicketById(id).then((data) => {
      this.ticket = data[0];
      this.servicios = data[0].servicio;
      this.catalogos = data[0].servicio.catalogo;
      this.personas = data[0].usuario.persona;
      this.prioridades = data[0].servicio.prioridad;
      this.usuarios = data[0].usuario;
      this.criticidad=data[0].servicio.criticidad;
      this.severidad=data[0].severidad;
      this.estado=data[0].estado;
      this.viaComunicacion=data[0].viaComunicacion;
      if (data[0].tecnico != null) {
        this.nombreTecnico = data[0].tecnico.persona;
      }
      this.buscarSolucion();
      this.nivelSeveridad=this.severidad.nivelSeveridad;
      this.getHistorialEscalar();
    });
  }
  escalatecnico:boolean;
  nivel1:any=[];
  getHistorialEscalar(){
    this.servicioTicket.getHistorialEscalar(this.ticket.codticket).then(data=>{
      if(data!=null){
        this.tecnicoEscala=data;
        this.nivel1=this.tecnicoEscala.nivel1.persona;
      }
      this.escalatecnico=Object.entries(this.tecnicoEscala).length !== 0;
    });
  }
  buscarSolucion(){

    this.solucionService.getSolucionById(this.ticket.codticket).then(data=>{
      this.solucionticket=data[0];
    });

  }
  cambiarEstado( id:any){
    this.servicioTicket.putCambiarEstado(2,id).then(data =>{});
  }

  async presentAlertaVerDescripcionTicket() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Detalle del ticket',
      subHeader: 'Descripcion:',
      message: this.ticket.descripcionTicket,
      buttons: ['OK'],
    });
    await alert.present();
  }
  async presentAlertaVerSolucionTicket() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Solucion del ticket',
      subHeader: 'Descripcion:',
      message: this.solucionticket.descripcion,
      buttons: ['OK'],
    });
    await alert.present();
  }
}
