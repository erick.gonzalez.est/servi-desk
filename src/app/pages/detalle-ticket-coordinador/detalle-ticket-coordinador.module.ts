import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleTicketCoordinadorPageRoutingModule } from './detalle-ticket-coordinador-routing.module';

import { DetalleTicketCoordinadorPage } from './detalle-ticket-coordinador.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleTicketCoordinadorPageRoutingModule
  ],
  declarations: [DetalleTicketCoordinadorPage]
})
export class DetalleTicketCoordinadorPageModule {}
