import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalleTicketCoordinadorPage } from './detalle-ticket-coordinador.page';

const routes: Routes = [
  {
    path: '',
    component: DetalleTicketCoordinadorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleTicketCoordinadorPageRoutingModule {}
