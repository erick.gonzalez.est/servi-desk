import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListaTicketCoordinadorPageRoutingModule } from './lista-ticket-coordinador-routing.module';

import { ListaTicketCoordinadorPage } from './lista-ticket-coordinador.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    ListaTicketCoordinadorPageRoutingModule
  ],
  declarations: [ListaTicketCoordinadorPage]
})
export class ListaTicketCoordinadorPageModule {}
