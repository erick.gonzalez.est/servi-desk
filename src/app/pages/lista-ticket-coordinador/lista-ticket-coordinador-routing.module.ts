import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaTicketCoordinadorPage } from './lista-ticket-coordinador.page';

const routes: Routes = [
  {
    path: '',
    component: ListaTicketCoordinadorPage
    
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaTicketCoordinadorPageRoutingModule {}
