import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListaTicketCoordinadorPage } from './lista-ticket-coordinador.page';

describe('ListaTicketCoordinadorPage', () => {
  let component: ListaTicketCoordinadorPage;
  let fixture: ComponentFixture<ListaTicketCoordinadorPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTicketCoordinadorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListaTicketCoordinadorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
