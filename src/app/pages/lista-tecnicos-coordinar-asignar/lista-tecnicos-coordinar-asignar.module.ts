import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListaTecnicosCoordinarAsignarPageRoutingModule } from './lista-tecnicos-coordinar-asignar-routing.module';

import { ListaTecnicosCoordinarAsignarPage } from './lista-tecnicos-coordinar-asignar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    ListaTecnicosCoordinarAsignarPageRoutingModule
  ],
  declarations: [ListaTecnicosCoordinarAsignarPage]
})
export class ListaTecnicosCoordinarAsignarPageModule {}
