import { LoadingController, ModalController, AlertController } from '@ionic/angular';
import { TecnicoService } from './../../services/tecnico.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-tecnicos-coordinar-asignar',
  templateUrl: './lista-tecnicos-coordinar-asignar.page.html',
  styleUrls: ['./lista-tecnicos-coordinar-asignar.page.scss'],
})
export class ListaTecnicosCoordinarAsignarPage implements OnInit {
  textoBusqueda:string='';
  comboTecnicos:any=[];
  constructor(public loadingController: LoadingController, private alertController:AlertController,private servicioTecnico:TecnicoService, private modalController:ModalController, private ionLoader: LoadingController) {
    this.descargandoDatos();
  }

  ngOnInit() {
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });

  }
  buscar(event){
    this.textoBusqueda=event.detail.value;
  }
  async dismiss2(tecnico:any) {

    const alert = await this.alertController.create({
      header: 'Asignar ticket',
      message: '¿Está seguro que quiere asignar a '+tecnico.nombres +' a este ticket? ',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Asignar',
          handler: () => {
            console.log('Confirm Okay');
            this.modalController.dismiss({
              'tecnico':tecnico,
              'dismissed': true
            });
          }
        }
      ]
    });
    await alert.present();
  }

  async descargandoDatos(){
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      message: 'Descargando datos...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    setTimeout(() => {
      this.getObtenerTecnicos();
    }, 1000);
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  nivel = parseInt(localStorage.getItem('nivel'));
  async getObtenerTecnicos(){
    this.servicioTecnico.GetTecnicos(this.nivel)
    .then(data => {
      this.comboTecnicos=data;
      console.log(this.comboTecnicos);
    });
     this.loadingController.dismiss();
  }
}
