import { Component, OnInit } from '@angular/core';
import { Chooser, ChooserResult } from '@ionic-native/chooser/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { ConsumoImagenService } from './../../services/consumo-imagen.service';

@Component({
  selector: 'app-modal-lista-tecnico',
  templateUrl: './modal-lista-tecnico.page.html',
  styleUrls: ['./modal-lista-tecnico.page.scss'],
})
export class ModalListaTecnicoPage implements OnInit {

  processing:boolean;
  uploadImage: string;
  img: String;
  fileName: any;
  fileObj: ChooserResult;

  constructor(private consumo:ConsumoImagenService, private fileChooser: FileChooser) { }
  archivo:File;
  photoSelected : string | ArrayBuffer;
  ngOnInit() {
  }
  uploadFile() {
    this.consumo.onClickSubir()
  }
  onChange(fileChangeEvent: { target: { files: File[]; }; }){
    this.archivo=fileChangeEvent.target.files[0];
    this.fileName = this.archivo.name;
    console.log(this.fileName);
    this.consumo.onChange(this.archivo);
  }

  presentActionSheet(fileLoader) {
    fileLoader.click();
    var that = this;
    fileLoader.onchange = function () {
      var file = fileLoader.files[0];
      var reader = new FileReader();

      // reader.addEventListener("load", function () {
      //   that.fileName = file.name;
      //   console.log(that.fileName);
      // }, false);
      // if (file) {
      //   reader.readAsDataURL(file);
      // }
    }
  }

  /////////////////////////////////////////////////////////////

removePic() {
  console.log("antes "+this.fileName);
  this.fileName = '';
  this.archivo = null;
  console.log("despues "+this.fileName);
}

}
