import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalListaTecnicoPage } from './modal-lista-tecnico.page';

const routes: Routes = [
  {
    path: '',
    component: ModalListaTecnicoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalListaTecnicoPageRoutingModule {}
