import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { PipesModule } from './pipes/pipes.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Chooser} from '@ionic-native/chooser/ngx';
import { FileTransfer} from '@ionic-native/file-transfer/ngx'
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    

  ],
  providers: [
    FileTransfer,
    FileChooser,
    FilePath,
    File,
    SQLite,
    SQLitePorter,
    EmailComposer,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Chooser
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

// ionic cap run android -l --host=192.168.1.50 --consolelogs --serverlogs
// edge://inspect/#devices